import styled from "styled-components";
import COLORS from "./colors";

export const FirstLogo = styled.img`
  width: 100%;
  object-fit: fill;
  margin-top: 92px;
  z-index: 2;

  @media (max-width: 950px) {
    margin-top: 64px;
  }
`;

export const SeInscrevaContainer = styled.div`
  width: 100%;
  height: 128px;
  background-color: ${COLORS.Pink};
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  @media (max-width: 950px) {
    height: 100px;
  }
`;

export const SeInscreva = styled.div`
  width: 100%;
  max-width: 1440px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.White};
  font-weight: 800;
  font-size: 24px;
  text-align: left;

  @media (max-width: 950px) {
    font-size: 18px;
    margin-left: 0px;
  }
`;

export const TwitchLogo = styled.img`
  width: 61px;
  height: 78px;
  margin-left: 32px;

  @media (max-width: 950px) {
    width: 24px;
    height: 36px;
    margin-left: 16px;
    margin-right: 16px;
  }
`;

export const FinalistasContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  background-color: ${COLORS.Dark};
`;

export const Finalistas = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  max-width: 1440px;
  flex-direction: column;
`;

export const FinalistaHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 52px;
  margin-bottom: 32px;
`;

export const Separator = styled.img`
  height: 30px;
  width: 54px;
  @media (max-width: 950px) {
    height: 20px;
    width: 38px;
  }
`;

export const FinalistaTitle = styled.text`
  color: ${COLORS.White};
  font-size: 40px;
  font-weight: 800;
  margin: 0px 24px;
  @media (max-width: 950px) {
    font-size: 32px;
  }
`;

export const FinalistaList = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 68px;
  @media (max-width: 950px) {
    margin-top: 32px;
  }
`;

export const Personageminscrever = styled.img`
  height: 128px;
  width: 360px;
  left: 0px;
  position: absolute;
  object-fit: cover;

  @media (max-width: 950px) {
    visibility: hidden;
  }
`;

export const FinalistaContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 0px 36px;
  @media (max-width: 950px) {
    margin: 0px 12px;
  }
`;

export const FinalistaImg = styled.img`
  width: 435px;
  height: 518px;
  object-fit: none;

  @media (max-width: 1200px) {
    width: 390px;
    height: 470px;
  }

  @media (max-width: 1050px) {
    width: 340px;
    height: 400px;
  }
`;

export const FinalistaBottom = styled.div`
  background-color: ${COLORS.Yellow};
  height: 100px;
  width: 435px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  color: ${COLORS.Dark};
  font-size: 20px;
  font-weight: 800;
  @media (max-width: 1200px) {
    width: 390px;
  }

  @media (max-width: 1050px) {
    width: 340px;
  }
`;

export const FinalistaLocal = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.Dark};
  margin-top: 8px;
  font-size: 16px;
  font-weight: 600px;
`;

export const FinalistaLocalImg = styled.img`
  margin-left: 10px;
  width: 45px;
  height: 33px;
`;

export const DMNContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const DMNContainerInside = styled.div`
  display: flex;
  max-width: 1440px;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  width: 100%;
`;

export const DMNHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 90px;
`;

export const DMNTitle = styled.text`
  color: ${COLORS.White};
  font-size: 40px;
  margin: 0px 24px;
  font-weight: 800;
`;

export const DMNText = styled.text`
  text-align: left;
  color: ${COLORS.White};
  font-size: 24px;
  font-weight: 500;
  padding: 0px 122px;
  margin-top: 48px;
  @media (max-width: 950px) {
    padding: 0px 24px;
    font-size: 18px;
    margin-top: 32px;
  }
`;

export const DMNImage = styled.img`
  padding: 0px 122px;
  width: 100%;
  object-fit: cover;
  margin-top: 64px;
  margin-bottom: 80px;

  @media (max-width: 950px) {
    padding: 0px 24px;
    margin-bottom: 60px;
    margin-top: 48px;
  }
`;

export const RegulamentoContainer = styled.div`
  width: 100%;
  background-color: ${COLORS.Yellow};
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const Regulamento = styled.div`
  width: 100%;
  max-width: 1440px;
  padding-top: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const RegulamentoHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const RegulamentoTitle = styled.text`
  color: ${COLORS.Dark};
  font-size: 40px;
  font-weight: 800;
  margin: 0px 24px;
  @media (max-width: 950px) {
    font-size: 32px;
  }
`;

export const RegulamentoPersonagem = styled.img`
  height: 120%;
  position: absolute;
  left: 0px;
  top: 0px;
  object-fit: cover;

  @media (max-width: 950px) {
    visibility: hidden;
  }
`;

export const RegulamentoBody = styled.div`
  margin-top: 60px;
  color: ${COLORS.Dark};
  font-size: 24px;
  font-weight: 500;
  padding: 0px 122px;
  padding-left: 250px;
  margin-bottom: 42px;
  position: relative;
  text-align: left;

  @media (max-width: 950px) {
    padding: 0px 32px;
    font-size: 18px;
    margin-top: 48px;
  }
`;

export const OutrasEdicoesContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${COLORS.Dark};
`;

export const OutrasEdicoes = styled.div`
  width: 100%;
  max-width: 1440px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;

  @media (max-width: 950px) {
    justify-content: center;
  }
`;

export const OutrasEdicoesHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 32px;
`;

export const OutrasEdicoesTitle = styled.text`
  color: ${COLORS.White};
  font-size: 40px;
  font-weight: 800;
  margin: 0px 24px;

  @media (max-width: 950px) {
    font-size: 24px;
  }
`;

export const OutrasEdicoesImg = styled.img`
  width: 100%;
  margin: 40px 0px;
  padding: 0px 122px;

  @media (max-width: 950px) {
    padding: 0px 24px;
  }
`;

export const OutrasEdicoesAnoList = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: ${COLORS.White};
  font-weight: 500;
  margin-bottom: 120px;
  padding: 0px 122px;
  font-size: 20px;
  @media (max-width: 950px) {
    font-size: 14px;
    margin-bottom: 32px;
  }
`;

export const Edicao = styled.text`
  margin-right: 60px;
  @media (max-width: 950px) {
    margin-right: 16px;
  }
`;

export const FooterContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${COLORS.Dark};
  position: relative;
`;

export const Footer = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  max-width: 1440px;
  padding: 0px 122px;

  @media (max-width: 950px) {
    padding: 0px 32px;
  }
`;

export const FooterTitle = styled.text`
  color: ${COLORS.White};
  font-size: 24px;
  font-weight: 800;
  margin-top: 24px;
  margin-bottom: 42px;
`;

export const FooterRedeSocialContainer = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  margin-bottom: 100px;
`;

export const FooterSocial = styled.img`
  height: 57px;
  width: 57px;
  margin-right: 14px;
  cursor: pointer;

  @media (max-width: 950px) {
    height: 32px;
    width: 32px;
  }
`;

export const LogoFooter = styled.img`
  height: 205px;
  width: 243px;
  position: absolute;
  right: 31px;
  bottom: 130px;

  @media (max-width: 950px) {
    visibility: hidden;
  }
`;

export const FooterBottom = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
  flex-direction: column;
  @media (max-width: 950px) {
    margin-top: 0px;
  }
`;

export const FooterBottomImg = styled.img`
  height: 58px;
  width: 111px;
  margin-bottom: 20px;
`;

export const FooterBottomText = styled.text`
  font-size: 16px;
  color: ${COLORS.White};
  margin-bottom: 24px;
  font-weight: 600;

  text-align: center;
  @media (max-width: 950px) {
    font-size: 14px;
  }
`;

