import Carousel from "react-elastic-carousel";

import headerSeparator from "./Assets/headerSeparator.svg";
import separator from "./Assets/separator.svg";
import twitch from "./Assets/twitch.svg";

import banner1 from "./Assets/banner1.png";
import banner2 from "./Assets/banner2.png";
import banner3 from "./Assets/banner3.png";
import personagem3 from "./Assets/personagem3.png";
import logofooter from "./Assets/logofooter.svg";
import facebook from "./Assets/facebook.svg";
import insta from "./Assets/insta.svg";
import twitter from "./Assets/twitter.svg";
import youtube from "./Assets/youtube.svg";
import twitchfooter from "./Assets/twitchfooter.svg";
import fdr from "./Assets/fdr.svg";
import alagoas from "./Assets/alagoas.svg";
import minas from "./Assets/minas.svg";
import distrito from "./Assets/distrito.svg";
import ALICE from "./Assets/ALICE.png";
import DIAS from "./Assets/DIAS.png";
import ALVES from "./Assets/ALVES.png";

import Card from "./components/finalist-card";
import Header from "./components/header";
import LinkBanner from "./components/link-banner";
import RegisterForm from "./components/register-form";

import {
  FooterRedeSocialContainer,
  RegulamentoPersonagem,
  LogoFooter,
  FooterBottom,
  FooterBottomImg,
  FooterBottomText,
  FooterSocial,
  Footer,
  FooterTitle,
  FooterContainer,
  OutrasEdicoesAnoList,
  Edicao,
  OutrasEdicoesImg,
  OutrasEdicoesTitle,
  OutrasEdicoesHeader,
  OutrasEdicoesContainer,
  RegulamentoTitle,
  OutrasEdicoes,
  RegulamentoBody,
  RegulamentoHeader,
  RegulamentoContainer,
  Regulamento,
  DMNImage,
  DMNText,
  DMNContainer,
  DMNContainerInside,
  FinalistaTitle,
  Separator,
  FinalistaHeader,
  Finalistas,
  FinalistasContainer,
  FirstLogo,
  TwitchLogo,
} from "./styles";
import React from "react";
import LINKS from "./links";

function App() {
  return (
    <div
      className="App"
      style={{
        maxWidth: "100vw",
        overflowX: "hidden",
      }}
    >
      <Header />

      {/** LOGO 1! */}
      <FirstLogo alt={"banner1"} src={banner1} />

      <LinkBanner url={LINKS.twitch}>
        SE INSCREVA NA TWITCH DA FDR <br /> E ACOMPANHE AS TRANSMISSÕES!
        <TwitchLogo src={twitch} alt={"twitch"} />
      </LinkBanner>

      {/** FINALISTAS! */}
      <FinalistasContainer id={"finalistas"}>
        {/** FINALISTA INSIDE */}
        <Finalistas>
          {/** HEADER */}
          <FinalistaHeader>
            <Separator src={separator} alt={"separator"} />
            <FinalistaTitle>FINALISTAS</FinalistaTitle>
            <Separator src={separator} alt={"separator"} />
          </FinalistaHeader>

          {/** FINALISTAS */}
          <Carousel
            breakPoints={[
              { width: 1, itemsToShow: 1 },
              { width: 550, itemsToShow: 2 },
              { width: 768, itemsToShow: 3 },
            ]}
            enableAutoPlay
            pagination={false}
          >
            {/** SUB FINALISTA */}
            <Card
              image={DIAS}
              name={"Dias"}
              state={"Minas Gerais"}
              stageImage={minas}
              content={""}
            />

            {/** FINALISTA UM */}
            <Card
              image={ALICE}
              name={"Alice Gorete"}
              state={"Alagoas"}
              stageImage={alagoas}
              content={""}
            />

            {/** SUB FINALISTA */}
            <Card
              image={ALVES}
              name={"Alves"}
              state={"Distrito Federal"}
              stateImage={distrito}
              content={""}
              extra={"TEste"}
            />
          </Carousel>
        </Finalistas>
      </FinalistasContainer>

      {/** DMN 2020 */}
      <DMNContainer id={"dmn"}>
        <DMNContainerInside>
          {/** HEADER */}
          <FinalistaHeader>
            <Separator src={separator} alt={"separator"} />
            <FinalistaTitle>DMN 2020</FinalistaTitle>
            <Separator src={separator} alt={"separator"} />
          </FinalistaHeader>

          {/** TEXT */}
          <DMNText>
            Lorem ipsum dolor sit amet, consectetur afaef rtttdipiscing elit.
            Sed fermentum, orci vel convallis interdum, lectgus lorem molestie
            ante, id faucibus sem nunc sed neque. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit. Sddesed fermentum, orci vel convallis
            interdum, lectus lorem molestie ante, id faucibusaaakkdk sem elit.
            Sddesed fermentum, orci vel convallis interdum, lectus lorem
            molestie ante, id faucibus sem.
            <br />
            <br />
            Convallis interdum, lectus lorem molestie ante, id faucibusaaakkdk
            sem elit.
          </DMNText>

          {/** IMAGEM */}

          <DMNImage src={banner2} alt={"banner2"} />
        </DMNContainerInside>
      </DMNContainer>

      <LinkBanner url={LINKS.twitch}>
        SE INSCREVA NA TWITCH DA FDR <br /> E ACOMPANHE AS TRANSMISSÕES!
        <TwitchLogo src={twitch} alt={"twitch"} />
      </LinkBanner>

      {/** REGULAMENTO */}
      <RegulamentoContainer id={"regulamento"}>
        <RegulamentoPersonagem src={personagem3} alt={"personagem3"} />
        {/** REGULAMENTO INSIDE */}
        <Regulamento>
          {/** HEADER */}
          <RegulamentoHeader>
            <Separator src={headerSeparator} alt={"separator"} />

            <RegulamentoTitle>REGULAMENTO</RegulamentoTitle>
            <Separator src={headerSeparator} alt={"separator"} />
          </RegulamentoHeader>
          {/** BODY */}
          <RegulamentoBody>
            Lorem ipsum dolor sit amet, consectetur afaef rtttdipiscing elit.
            Sed fermentum, orci vel convallis interdum, lectgus lorem molestie
            ante, id faucibus sem nunc sed neque. Pellentesque ladcoreet
            tedddmpus sapien. Fusce non enim lacus. Curabitffffffur at sapifffen
            vel magna accumsan euismod quis sed tortor. Dodddgddnec hendrerit
            pretium auctor. Praesent aliquam tempor orci a pelledntesque. Lorem
            ipsum dolor sit amet, consectetur adipiscing eddlit. Etiam finibus
            ante quis consequat aliquam. Vivamus sit amet lacusd a orci suscipit
            convallis vel nec metus.
            <br />
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sddesed
            fermentum, orci vel convallis interdum, lectus lorem molestie
            ante,id faucibus sem.
          </RegulamentoBody>
        </Regulamento>
      </RegulamentoContainer>

      {/** OUTRAS EDICOES! */}
      <OutrasEdicoesContainer id={"outrasedicoes"}>
        {/** OUTRAS EDICOES INSIDE */}
        <OutrasEdicoes>
          {/** HEADER */}
          <OutrasEdicoesHeader>
            <Separator src={separator} alt={"separator"} />

            <OutrasEdicoesTitle>OUTRAS EDIÇÕES</OutrasEdicoesTitle>
            <Separator src={separator} alt={"separator"} />
          </OutrasEdicoesHeader>
          {/** IMAGE */}
          <OutrasEdicoesImg src={banner3} alt={"banner3"} />

          {/** OPTIONS */}
          <OutrasEdicoesAnoList>
            <Edicao>2012</Edicao>
            <Edicao>2013</Edicao>
            <Edicao>2014</Edicao>
            <Edicao>2015</Edicao>
            <Edicao>2016</Edicao>
            <Edicao>2017</Edicao>
            <Edicao>2018</Edicao>
            <Edicao>2019</Edicao>
          </OutrasEdicoesAnoList>
        </OutrasEdicoes>
      </OutrasEdicoesContainer>

      <LinkBanner url={LINKS.store}>ACESSE A NOSSA LOJA E CONFIRA</LinkBanner>
      <RegisterForm />

      {/** FOOTER */}
      <FooterContainer>
        {/** FOOTER INSIDE! */}
        <Footer>
          <FooterTitle>DUELO DE MCS NACIONAL</FooterTitle>
          <LogoFooter src={logofooter} alt={""} />

          {/** REDES SOCIAIS! */}
          <FooterRedeSocialContainer>
            <FooterSocial src={facebook} alt={"facebook"} />
            <FooterSocial src={insta} alt={"insta"} />
            <FooterSocial src={twitter} alt={"twitter"} />
            <FooterSocial src={youtube} alt={"youtube"} />
            <FooterSocial src={twitchfooter} alt={"twitchfooter"} />
          </FooterRedeSocialContainer>

          {/** FINAL FOOTER */}
          <FooterBottom>
            <FooterBottomImg src={fdr} alt={"fdr"} />

            <FooterBottomText>
              DUELO DE MCS NACIONAL 2020 © ALGUNS DIREITOS RESERVADOS
            </FooterBottomText>
          </FooterBottom>
        </Footer>
      </FooterContainer>
    </div>
  );
}

export default App;
