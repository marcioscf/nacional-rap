const COLORS = {
  Dark: "#000",
  DarkYellow: "#d5d600",
  Yellow: "#FEFF01",
  Pink: "#FC00ED",
  White: "#fff",
  DarkPink: "#a8009e",
};

export default COLORS;
