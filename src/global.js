import { createGlobalStyle } from "styled-components";
import COLORS from "./colors";

export default createGlobalStyle`
* {
    margin: 0;
    font-family: obviously, sans-serif;
    padding: 0;
    box-sizing: border-box;
    scroll-behavior: smooth;
}

body {
    background: ${COLORS.Dark};
    font-family: obviously, sans-serif;
    scroll-behavior: smooth;
}
`