import styled from 'styled-components'
import COLORS from '../../colors'

export const Logo = styled.img`
    width: 220px;
    height: auto;
    position: absolute;
    left: -40px;
    top: 0px;
    object-fit: fill;
    @media(max-width: 950px) {
        visibility: hidden;
    }
`

export const NavbarContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 92px;
    background-color: ${COLORS.Yellow};
    position: fixed;
    z-index: 10;
    @media(max-width: 950px) {
        height: 64px;
    }
`

export const Navbar = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-end;
    width: 100%;
    max-width: 1440px;
    padding: 0px 32px;
    font-weight: 800;
    @media(max-width: 950px) {
        visibility: hidden;
    }
    
`

export const NavbarBurguerContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    flex-direction: column;
    position: absolute;
    top: 64px;
    font-size: 20px;
    padding: 4px;
    font-weight: 600;
    background-color: ${COLORS.Yellow};
    visibility: hidden;
    @media(max-width: 950px) {
        visibility: visible;
    }
`

export const Navburguer = styled.img`
visibility: hidden;
height: 32px;
width: 32px;
cursor: pointer;
    @media(max-width: 950px) {
        visibility: visible;
        height: 32px;
        width: 32px;
        margin-right: 70px;
    }
`

export const HeaderSeparator = styled.img`
    height: 10px;
    width: 21px;
    margin: 0px 20px;
`

export const NavbarSocial = styled.div`
    position: absolute;
    right: 32px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    flex-direction: column;
    top: 124px;

    @media(max-width: 950px) {
        visibility: hidden;
        opacity: 0;
    }
`

export const NavbarSocialButton = styled.img`
    height: 48px;
    width: 48px;
    margin-bottom: 12px;
    cursor: pointer;
`
