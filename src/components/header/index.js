import React, { useState } from "react";

import {
  Navbar,
  NavbarBurguerContainer,
  NavbarContainer,
  NavbarSocial,
  NavbarSocialButton,
  Navburguer,
  HeaderSeparator,
  Logo,
} from "./styled";

import menu from "../../Assets/menu.svg";
import search from "../../Assets/search.svg";
import logo from "../../Assets/logo.png";
import facebook from "../../Assets/facebook.svg";
import insta from "../../Assets/insta.svg";
import twitter from "../../Assets/twitter.svg";
import youtube from "../../Assets/youtube.svg";
import headerSeparator from "../../Assets/headerSeparator.svg";
import twitchfooter from "../../Assets/twitchfooter.svg";

import LINKS from "../../links";

const Header = () => {
  const [burguerOpen, setBurguerOpen] = useState(false);
  return (
    <NavbarContainer>
      {/** NAV BAR INTERNO */}
      <Navbar>
        <text
          style={{
            cursor: "pointer",
          }}
          onClick={() => {
            document
              .getElementById("finalistas")
              .scrollIntoView({ block: "end" });
          }}
        >
          FINALISTA
        </text>
        <HeaderSeparator alt={""} src={headerSeparator} />
        <text
          style={{
            cursor: "pointer",
          }}
          onClick={() => {
            document.getElementById("dmn").scrollIntoView();
          }}
        >
          DMN 2020
        </text>
        <HeaderSeparator alt={""} src={headerSeparator} />
        <text
          style={{
            cursor: "pointer",
          }}
          onClick={() => {
            document
              .getElementById("regulamento")
              .scrollIntoView({ block: "end" });
          }}
        >
          REGULAMENTO
        </text>
        <HeaderSeparator alt={""} src={headerSeparator} />
        <text
          style={{
            cursor: "pointer",
          }}
          onClick={() => {
            document.getElementById("outrasedicoes").scrollIntoView();
          }}
        >
          OUTRAS EDIÇÕES
        </text>
        <HeaderSeparator alt={""} src={headerSeparator} />
        <text
          style={{
            marginRight: 48,
            cursor: "pointer",
          }}
          onClick={() => {
            document
              .getElementById("cadastrese")
              .scrollIntoView({ block: "end" });
          }}
        >
          CADASTRE-SE
        </text>
        <img
          alt={"search"}
          src={search}
          style={{
            height: 41,
            width: 41,
          }}
        />
      </Navbar>

      {/** NAVBAR REDE SOCIAL */}
      <NavbarSocial>
        <NavbarSocialButton
          src={facebook}
          alt={"facebook"}
          onClick={() => {
            window.open(LINKS.face, "_blank").focus();
          }}
        />
        <NavbarSocialButton
          src={insta}
          alt={"insta"}
          onClick={() => {
            window.open(LINKS.insta, "_blank").focus();
          }}
        />
        <NavbarSocialButton
          src={twitter}
          alt={"twitter"}
          onClick={() => {
            window.open(LINKS.twitter, "_blank").focus();
          }}
        />
        <NavbarSocialButton
          src={youtube}
          alt={"youtube"}
          onClick={() => {
            window.open(LINKS.youtube, "_blank").focus();
          }}
        />
        <NavbarSocialButton
          src={twitchfooter}
          alt={"twitchfooter"}
          onClick={() => {
            window.open(LINKS.twitch, "_blank").focus();
          }}
        />
      </NavbarSocial>

      {/** LOGO */}
      <Logo src={logo} alt={"logo"} />
      <Navburguer
        alt={"menu"}
        src={menu}
        onClick={() => {
          setBurguerOpen(!burguerOpen);
        }}
      />

      {/** Menu burguer */}
      {burguerOpen && (
        <NavbarBurguerContainer>
          <text
            style={{
              borderBottom: `solid 1px #000`,
              paddingBottom: 8,
              paddingTop: 4,
              width: "100%",
              cursor: "pointer",
            }}
            onClick={() => {
              setBurguerOpen(false);
              document
                .getElementById("finalistas")
                .scrollIntoView({ block: "end" });
            }}
          >
            FINALISTA
          </text>
          <text
            onClick={() => {
              setBurguerOpen(false);
              document.getElementById("dmn").scrollIntoView();
            }}
            style={{
              borderBottom: `solid 1px #000`,
              paddingBottom: 8,
              paddingTop: 4,
              width: "100%",
              cursor: "pointer",
            }}
          >
            DMN 2020
          </text>
          <text
            onClick={() => {
              setBurguerOpen(false);
              document
                .getElementById("regulamento")
                .scrollIntoView({ block: "end" });
            }}
            style={{
              borderBottom: `solid 1px #000`,
              paddingBottom: 8,
              paddingTop: 4,
              width: "100%",
              cursor: "pointer",
            }}
          >
            REGULAMENTO
          </text>
          <text
            onClick={() => {
              setBurguerOpen(false);
              document.getElementById("outrasedicoes").scrollIntoView();
            }}
            style={{
              borderBottom: `solid 1px #000`,
              paddingBottom: 8,
              paddingTop: 4,
              width: "100%",
              cursor: "pointer",
            }}
          >
            OUTRAS EDIÇÕES
          </text>
          <text
            style={{
              paddingBottom: 8,
              paddingTop: 4,
              width: "100%",
              cursor: "pointer",
            }}
            onClick={() => {
              setBurguerOpen(false);
              document
                .getElementById("cadastrese")
                .scrollIntoView({ block: "end" });
            }}
          >
            CADASTRE-SE
          </text>
        </NavbarBurguerContainer>
      )}
    </NavbarContainer>
  );
};

export default Header;
