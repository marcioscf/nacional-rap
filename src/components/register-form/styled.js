import styled from "styled-components";
import COLORS from "../../colors";

export const CadastrarContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${COLORS.Yellow};
  position: relative;
`;

export const Separator = styled.img`
  height: 30px;
  width: 54px;
  @media (max-width: 950px) {
    height: 20px;
    width: 38px;
  }
`;

export const PersonagemDois = styled.img`
  position: absolute;
  height: 927px;
  width: 638px;
  right: 64px;
  top: -36px;
  object-fit: fill;
  @media (max-width: 950px) {
    visibility: hidden;
  }
  @media (max-width: 1250px) {
    right: -64px;
  }
`;

export const Cadastrar = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 0px 122px;
  flex-direction: column;
  max-width: 1440px;
  @media (max-width: 950px) {
    padding: 0px 32px;
    align-items: center;
  }
`;

export const CadastrarHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 52px;
  margin-bottom: 80px;
  @media (max-width: 950px) {
    margin-top: 32px;
    margin-bottom: 64px;
  }
`;

export const CadastrarTitle = styled.text`
  color: ${COLORS.Dark};
  font-size: 40px;
  font-weight: 800;
  margin: 0px 24px;
  @media (max-width: 950px) {
    font-size: 24px;
    margin: 0px 18px;
  }
`;

export const CadastrarInputContainer = styled.div`
  border-bottom: solid 2px ${COLORS.Dark};
  padding-bottom: 8px;
  width: 50%;
  margin-bottom: 32px;
  @media (max-width: 950px) {
    width: 100%;
  }
`;

export const CadastrarInput = styled.input`
  border: none;
  background-color: ${COLORS.Yellow};
  font-size: 22px;
  font-weight: 600;
  color: ${COLORS.Dark};
  outline: none;
`;

export const CadastrarButton = styled.button`
  padding: 8px 43px;
  border: solid 3px ${COLORS.Dark};
  color: ${COLORS.Dark};
  font-size: 22px;
  font-weight: 600;
  margin-bottom: 148px;
  margin-top: 16px;
  background: ${COLORS.Yellow};
  cursor: pointer;
  transition: 0.25s linear;
  &:hover {
    background: ${COLORS.DarkYellow};
  }
`;

export const CheckBox = styled.label``;
