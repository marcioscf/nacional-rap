import React, {useState} from "react";

import personagem2 from "../../Assets/personagem2.png";
import headerSeparator from "../../Assets/headerSeparator.svg";

import {
  CadastrarContainer,
  CadastrarInput,
  CadastrarButton,
  CadastrarInputContainer,
  CadastrarTitle,
  CadastrarHeader,
  Cadastrar,
  PersonagemDois,
  Separator,
  CheckBox
} from "./styled";

const RegisterForm = () => {
  const [checked, setChecked] = useState(false);
  return (
    <CadastrarContainer id={"cadastrese"}>
      {/** CADASTRE-SE INSIDE */}
      <Cadastrar>
        <PersonagemDois src={personagem2} alt={"personagem2"} />
        {/** IMAGEM */}
        {/** HEADER */}
        <CadastrarHeader>
          <Separator src={headerSeparator} alt={"separator"} />
          <CadastrarTitle>CADASTRE-SE</CadastrarTitle>
          <Separator src={headerSeparator} alt={"separator"} />
        </CadastrarHeader>

        {/** INPUTS */}

        {/** Nome */}
        <CadastrarInputContainer>
          <CadastrarInput type={"text"} placeholder={"NOME"} />
        </CadastrarInputContainer>

        {/** E-MAIL */}
        <CadastrarInputContainer>
          <CadastrarInput type={"text"} placeholder={"E-MAIL"} />
        </CadastrarInputContainer>

        {/** TELEFONE */}
        <CadastrarInputContainer>
          <CadastrarInput type={"text"} placeholder={"TELEFONE"} />
        </CadastrarInputContainer>

        <CheckBox>
          <input type="checkbox" checked={checked} onChange={() => {setChecked(!checked)}} />
                  {"  "}Li e concordo com os Termos e Condicoes
        </CheckBox>

        {/** BUTTON CADASTRE-SE */}
        <CadastrarButton>CADASTRE</CadastrarButton>
      </Cadastrar>
    </CadastrarContainer>
  );
};

export default RegisterForm;
