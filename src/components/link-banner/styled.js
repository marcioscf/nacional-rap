import styled from "styled-components";
import COLORS from "../../colors";

export const SeInscrevaContainer = styled.div`
  width: 100%;
  height: 128px;
  background-color: ${COLORS.Pink};
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  transition: 0.25s linear;
  cursor: pointer;
  overflow: hidden;
  @media (max-width: 950px) {
    height: 100px;
  }
  &:hover {
    background-color: ${COLORS.DarkPink};
  }
`;

export const Personageminscrever = styled.img`
  width: 300px;
  left: 0px;
  bottom: -214px;
  position: absolute;
  object-fit: cover;

  @media (max-width: 950px) {
    visibility: hidden;
  }
`;

export const SeInscreva = styled.div`
  width: 100%;
  max-width: 1440px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.White};
  font-weight: 800;
  font-size: 24px;
  text-align: left;

  @media (max-width: 950px) {
    font-size: 18px;
    margin-left: 0px;
  }
`;

export const TwitchLogo = styled.img`
  width: 61px;
  height: 78px;
  margin-left: 32px;

  @media (max-width: 950px) {
    width: 24px;
    height: 36px;
    margin-left: 16px;
    margin-right: 16px;
  }
`;
