import React from "react";
import {
  SeInscrevaContainer,
  SeInscreva,
  Personageminscrever,
} from "./styled";

import personagem from "../../Assets/personagem.png";

const LinkBanner = ({children, url}) => {
  return (
    <SeInscrevaContainer onClick={() => {window.open(url, '_blank').focus();}}>
      {/** SE INSCREVA NA TWITCH INSIDE! */}
      <SeInscreva>
        <Personageminscrever alt={""} src={personagem} />
        {children}
      </SeInscreva>
    </SeInscrevaContainer>
  );
};

export default LinkBanner;
