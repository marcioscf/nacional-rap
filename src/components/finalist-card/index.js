import React, { useState } from "react";

import {
  SubFinalBottom,
  SubFinalistaContainer,
  SubFinalistaImg,
  SubFinalLocal,
  SubFinalLocalText,
  SubFinalLocalImg,
  SubFinalistaContent,
} from "./styled";

const Card = ({
  image,
  name,
  stateImage,
  state,
  content,
  age,
  insta,
  extra,
}) => {
  const [contentShow, setContent] = useState(false);
  const handle = () => {
    setContent(!contentShow);
  };
  if (contentShow) {
    return (
      <SubFinalistaContainer
        onClick={handle}
        onMouseEnter={handle}
        onMouseLeave={handle}
      >
        <SubFinalistaContent>
          <h2>{name}</h2>
          <br />
          <h3>{age}</h3>
          <br />
          <h3>{state}</h3>
          <br />
          <h3>{insta}</h3>
          <br />
          {extra ? <h3>{extra}</h3> : null}
          {content}
        </SubFinalistaContent>
      </SubFinalistaContainer>
    );
  }
  return (
    <SubFinalistaContainer
      onClick={handle}
      onMouseEnter={handle}
      onMouseLeave={handle}
    >
      <SubFinalistaImg src={image} alt={name} />

      <SubFinalBottom>
        {name}
        <SubFinalLocal>
          <SubFinalLocalText>{state}</SubFinalLocalText>
          <SubFinalLocalImg src={stateImage} alt={state} />
        </SubFinalLocal>
      </SubFinalBottom>
    </SubFinalistaContainer>
  );
};

export default Card;
