import styled from "styled-components";
import COLORS from "../../colors";

export const SubFinalistaContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.Dark};
  flex-direction: column;
  margin-top: 48px;
`;

export const SubFinalistaContent = styled.div`
  height: 492px;
  width: 344px;

  background-color: #feff01;
  color: black;
  padding: 15px;
  
  @media (max-width: 1200px) {
    height: 432px;
    width: 290px;
  }

  @media (max-width: 1050px) {
    height: 362px;
    width: 240px;
  }
`;

export const SubFinalistaImg = styled.img`
  height: 410px;
  width: 344px;
  @media (max-width: 1200px) {
    height: 350px;
    width: 290px;
  }

  @media (max-width: 1050px) {
    height: 280px;
    width: 240px;
  }
`;

export const SubFinalBottom = styled.div`
  background-color: ${COLORS.Yellow};
  height: 82px;
  width: 344px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 20px;
  font-weight: 800;
  @media (max-width: 1200px) {
    width: 290px;
  }
  @media (max-width: 1050px) {
    width: 240px;
  }
  color: ${COLORS.Dark};
`;

export const SubFinalLocal = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.Dark};
  margin-top: 4px;
`;

export const SubFinalLocalText = styled.text`
  font-size: 16px;
  font-weight: 600;
  margin-right: 10px;
`;

export const SubFinalLocalImg = styled.img`
  width: 32px;
  height: 25px;
`;
